// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// import "cypress-file-upload";

// const AutomationCatalog = require("../App-Automation_Studio/AutomationCatalog");
// const HomePage = require("../App-Automation_Studio/home");

// // Click Home Page button to launch a page
// Cypress.Commands.add("SelectPageIcon", (PageName) => {
//   cy.get(HomePage.page.pagebuttons).each((Button, index) => {
//     if (Button.text().includes(PageName)) {
//       cy.get(HomePage.page.pagebuttons).eq(index).click();
//     }
//   });
// });
// // Click Workflow from Left Navigation of Automation Catalog/Automation Studio
// Cypress.Commands.add("SelectWorkflow", (WorkflowName) => {
//   cy.get(AutomationCatalog.page.existingworkflow).each((workflow, index) => {
//     if (workflow.text().includes(WorkflowName)) {
//       cy.get(AutomationCatalog.page.existingworkflow).eq(index).click();
//     }
//   });
// });
// // Validate Page Header
// Cypress.Commands.add("ValidatePageHeader", (PageName) => {
//   cy.get(HomePage.page.header).should("have.text", PageName);
// });
