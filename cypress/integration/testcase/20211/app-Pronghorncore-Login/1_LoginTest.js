/// <reference types="cypress" />

describe("Login Tests", function () {
  let timeout;
  beforeEach(() => {
    cy.fixture("LoginData").then(function (LoginData) {
      this.LoginData = LoginData;
      let timeout = 20000;
    });
  });

  it("Validate Login Page Elements", function () {
    cy.visit(Cypress.env("20211URL"));
    cy.get("#logo").should("be.visible");
    cy.get(".loginTitle").should("be.visible");
    cy.get("input[name=username]").should("be.visible");
    cy.get("input[name=password]").should("be.visible");
    cy.get("#sign_in").should("be.visible");
    cy.get(".footer .tagline").should("be.visible");
    cy.get(".footer .copyright").should("be.visible");
  });

  it("Validate Login with no credentials", function () {
    // cy.visit(this.LoginData.URL20211);
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Username Field Required";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Validate Login with no username", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=password]").type(this.LoginData.LoginPassword);
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Username Field Required";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Validate Login with no password", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=username]").type(this.LoginData.LoginUsername);
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Password Field Required";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Validate Login with invalid credentials", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=username]").type("invalidusername");
    cy.get("input[name=password]").type("invalidpassword");
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Invalid Credentials";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Validate Login with invalid username and valid password", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=username]").type("invalidusername");
    cy.get("input[name=password]").type(this.LoginData.LoginPassword);
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Invalid Credentials";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Validate Login with valid username and invalid password", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=username]").type(this.LoginData.LoginUsername);
    cy.get("input[name=password]").type("invalidpassword");
    cy.get("#sign_in").click();
    const LoginErrorMessage = "Invalid Credentials";
    cy.get("#output_message")
      .invoke("text")
      .should("have.string", LoginErrorMessage);
  });

  it("Login with Valid Credentials", function () {
    cy.visit(this.LoginData.URL20211);
    cy.get("input[name=username]").type(this.LoginData.LoginUsername);
    cy.get("input[name=password]").type(this.LoginData.LoginPassword);
    cy.get("#sign_in").click();
    cy.get("ph-rodeo > .text", { timeout: 10000 }).should("be.visible");
  });
});
