const logIn = require('../pronghorn-core/logIn');
const AutomationStudio = require('./AutomationStudio');

describe('Automation Studio @2020.1', function () {
  let timeout;
  // Login to Application with valid credentials
  describe('Automation Studio @2020.1', function () {
    beforeEach(function () {
      timeout = 40000;
      cy.visit('/');
          const username = Cypress.env('IAP_USERNAME');
      const password = Cypress.env('IAP_PASSWORD');
      cy.get('input[name=username]').type(username);
      cy.get('input[name=password]').type(password);
      
      cy.get(logIn.signinButton).click();
    });
    // Validate Automation Studio launch from Home Page
    describe('Automation Launch Point @2020.1', function () {
      it('should be displayed in Home Page.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.visit('/login?logout=true');
      });
    });
    // Validate Grid View of Automation Studio Page
    describe('Clicking Grid View of Automations link in Left Menu @2020.1 ', function () {
      it('Should display the automations in Grid View.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(
          AutomationStudio.automationstudiopage.AutomationsNavBarLink
        ).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewIcon).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewPageHeader).should(
          'have.text',
          'Automations'
        );
        cy.visit('/login?logout=true');
      });
    });
    // Validate Grid View of Forms Page
    describe('Clicking Grid View of Forms link in Left Menu @2020.1 ', function () {
      it('Should display the Forms in Grid View.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.automationstudiopage.FormsNavBarLink).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewIcon).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewPageHeader).should(
          'have.text',
          'Forms'
        );
        cy.visit('/login?logout=true');
      });
    });
    // Validate Grid View of Command Templates Page
    describe('Clicking Grid View of Command Templates link in Left Menu @2020.1 ', function () {
      it('Should display the Command Templates in Grid View.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(
          AutomationStudio.automationstudiopage.CommandTemplateslink
        ).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewIcon).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewPageHeader).should(
          'have.text',
          'Command Templates'
        );
        cy.visit('/login?logout=true');
      });
    });
    // Validate Grid View of Analytic Templates Page
    describe('Clicking Grid View of Analytic Templates link in Left Menu @2020.1 ', function () {
      it('Should display the Analytic Templates in Grid View.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(
          AutomationStudio.automationstudiopage.AnalyticTemplateslink
        ).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewIcon).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewPageHeader).should(
          'have.text',
          'Analytic Templates'
        );
        cy.visit('/login?logout=true');
      });
    });
    // Validate Grid View of Templates Page
    describe('Clicking Grid View of Templates link in Left Menu @2020.1 ', function () {
      it('Should display the Templates in Grid View.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.automationstudiopage.Templateslink)
          .eq(2)
          .click();
        cy.get(AutomationStudio.automationstudiopage.GridViewIcon).click();
        cy.get(AutomationStudio.automationstudiopage.GridViewPageHeader).should(
          'have.text',
          'Templates'
        );
        cy.visit('/login?logout=true');
      });
    });
  });
});
