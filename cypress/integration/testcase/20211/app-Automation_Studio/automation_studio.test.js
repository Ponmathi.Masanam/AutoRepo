const as = require('.');

describe('Given a user visits Automation Studio', function () {
  before(function () {
    this.iapRelease = Cypress.env('IAP_RELEASE');
  });

  beforeEach(function () {
    cy.visit(as.url);
  });

  describe('click Find An Automation button', function () {
    beforeEach(function () {
      cy.get(as.buttonFindAnAutomation).click();
    });

    if (this.iapRelease !== '2020.2') {
      it('should reveal the Collection dialog.', function () {
        cy.get(as.dialogCollection.buttonClose).should('be.visible');
      });

      describe('click Close button', function () {
        beforeEach(function () {
          cy.get(as.dialogCollection.buttonClose).click();
        });

        it('should hide the Collection dialog.', function () {
          cy.get(as.dialogCollection.buttonClose).should('not.be.visible');
        });
      });
      // Navigate among tabs of Collection dialog
      describe('Navigate among tabs of collection dialog', function () {
        it('should display the relevant data.', function () {
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .each((tab, index) => {
              cy.get(as.dialogCollection.tabscollection)
                .find(as.dialogCollection.tabs)
                .eq(index)
                .click();
            });
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(0)
            .should('have.text', 'AUTOMATIONS');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(1)
            .should('have.text', 'FORMS');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(2)
            .should('have.text', 'JSON FORMS');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(3)
            .should('have.text', 'COMMAND TEMPLATES');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(4)
            .should('have.text', 'ANALYTIC TEMPLATES');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(5)
            .should('have.text', 'TEMPLATES');
          cy.get(as.dialogCollection.tabscollection)
            .find(as.dialogCollection.tabs)
            .eq(6)
            .should('have.text', 'TRANSFORMATIONS');
        });
      });
      // Search for Classic Workflow
      describe('Classic Workflow search', function () {
        it('should display the name prefixed with icon.', function () {
          const workflowname = 'ClassicWF';
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
          cy.get(as.dialogCollection.ClassicWFIcon).should('be.visible');
        });
      });
      // Search for Generation 2 Workflow
      describe('Generation 2 Workflow search', function () {
        it('should not display any icon in its name.', function () {
          const workflowname = 'Gen2WF';
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
          cy.get(as.dialogCollection.ClassicWFIcon).should('not.exist');
        });
      });
      // Search with Invalid Workflow
      describe('Invalid Workflow search', function () {
        it('should not retrieve any search results.', function () {
          const workflowname = '!@#$';
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.ZeroTotalCount).should(
            'have.text',
            'Total: 0'
          );
        });
      });
      // Select multiple workflows -> Delete -> Cancel Delete confirmation dialog
      describe('Click Cancel of Delete Multiple Workflows', function () {
        it('should hide the Delete Automation dialog.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          cy.get(as.dialogCollection.SelectAllWF).click();
          cy.get(as.dialogCollection.DeleteIcon).click();
          // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000);
          cy.get(as.dialogCollection.DeleteDialogHeader).should('be.visible');
          cy.get(as.dialogCollection.buttonCancelDelete).click();
          cy.get(as.dialogCollection.buttonClose).click();
        });
      });

   describe('Click Cancel of Delete Selected Workflow', function () {
        it('should hide the Delete Automation dialog.', function () {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000);
        const workflowname = "ClassicWF";
        cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
        cy.get(as.dialogCollection.SearchIcon).click();
        cy.get(as.dialogCollection.SearchResultWFName).should('have.text',workflowname);
         cy.get(as.dialogCollection.Menulist).click();

        cy.get('span:contains("Delete"):visible').click();
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000);
        cy.get(as.dialogCollection.DeleteDialogHeader).should('be.visible');
        cy.get(as.dialogCollection.buttonCancelDelete).click();
        
        cy.get(as.dialogCollection.buttonClose).click();
        });  
      }); 

      describe('Deleting Selected Workflow', function () {
        it('should be permanent and cannot be undone.', function () {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000);
        const workflowname = "DeleteWF1";
        cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
        cy.get(as.dialogCollection.SearchIcon).click();
        cy.get(as.dialogCollection.SearchResultWFName).should('have.text',workflowname);
         cy.get(as.dialogCollection.Menulist).click();

        cy.get('span:contains("Delete"):visible').click();
        cy.get(as.dialogCollection.DeleteDialogHeader).should('be.visible');
        cy.get('.p-button.p-component.ph-button.ph-primary.p-button-text-only span').click();
        cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
        cy.get(as.dialogCollection.SearchIcon).click();
        cy.get(as.dialogCollection.ZeroTotalCount).should('have.text', 'Total: 0');   
        cy.get(as.dialogCollection.buttonClose).click();
        });  
      }); 
      // Validate Cancel button of Clone confirmation Dialog
      describe('Click Cancel of Clone Workflow', function () {
        it('should hide the Clone Automation dialog.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          const workflowname = 'ClassicWF';
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
          cy.get(as.dialogCollection.Menulist).click();

          cy.get(as.dialogCollection.Clone).click();
          cy.get(as.dialogCollection.CloneDialogHeader).should('be.visible');
          cy.get(as.dialogCollection.buttonCancelClone).click();
        });
      });
      // Validate Edit of Workflow
      describe('Click Edit of Workflow', function () {
        it('should launch the workflow builder of Automation.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          const workflowname = 'ClassicWF';
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
          cy.get(as.dialogCollection.Menulist).click();

          cy.get(as.dialogCollection.Edit).click();
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          cy.get(as.dialogCollection.EditWFHeader).should(
            'have.text',
            `Automation: ${workflowname}`
          );
          cy.visit(as.url);
        });
      });
    } else {
      it('should reveal the Automations page.', function () {
        cy.get(as['20201'].automations.title).should('be.visible');
      });
    }
  });

  describe('click Create An Automation button', function () {
    beforeEach(function () {
      cy.get(as.buttonCreateAnAutomation).click();
    });

    if (this.iapRelease !== '2020.2') {
      it('should reveal the Create dialog.', function () {
        cy.get(as.dialogCreate.buttonCancel).should('be.visible');
      });

      describe('click Cancel button', function () {
        beforeEach(function () {
          cy.get(as.dialogCreate.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as.dialogCreate.buttonCancel).should('not.be.visible');
        });
      });

      describe('Classic Workflow Creation', function () {
        it('should be done selecting Generation 1 Classic Option.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          const workflowname = 'Generation1WF';
          cy.get(as.dialogCreate.CreateAutomationTextbox).type(workflowname);
          cy.get(as.dialogCreate.NewWFCreatebutton).click();
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          cy.get(as.dialogCollection.EditWFHeader).should(
            'have.text',
            `Automation: ${workflowname}`
          );
          cy.visit(as.url);
          cy.get(as.buttonFindAnAutomation).click();
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
        });
      });

      describe('Gen2 Workflow Creation', function () {
        it('should be done selecting Generation 2 Cloud Option.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          const workflowname = 'Generation2WF';
          cy.get(as.dialogCreate.Gen2WFIcon).click();
          cy.get(as.dialogCreate.CreateAutomationTextbox).type(workflowname);
          cy.get(as.dialogCreate.NewWFCreatebutton).click();
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          cy.get(as.dialogCreate.NewWFHeader).should('have.text', workflowname);
          cy.visit(as.url);
          cy.get(as.buttonFindAnAutomation).click();
          cy.get(as.dialogCollection.SearchTextbox).type(workflowname);
          cy.get(as.dialogCollection.SearchIcon).click();
          cy.get(as.dialogCollection.SearchResultWFName).should(
            'have.text',
            workflowname
          );
        });
      });
    } else {
      it('should reveal the Create Automaton dialog.', function () {
        cy.get(as['20201'].dialogCreateAutomation.buttonCancel).should(
          'be.visible'
        );
      });

      describe('click Cancel button @ph78144', function () {
        beforeEach(function (done) {
          // Make exception not fail this test. At least until PH-78144 is addressed.
          cy.on('uncaught:exception', (error) => {
            expect(error.message).to.include("Failed to execute 'removeChild'");
            done();
            return false;
          });
          cy.get(as['20201'].dialogCreateAutomation.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as['20201'].dialogCreateAutomation.buttonCancel).should(
            'not.be.visible'
          );
        });
      });
    }
  });

  if (this.iapRelease === '2020.2') {
    describe('click Create button', function () {
      beforeEach(function () {
        cy.get(as.buttonCreate).click();
      });

      it('should reveal the Create dialog.', function () {
        cy.get(as.dialogCreate.buttonCancel).should('be.visible');
      });

      describe('click Cancel button', function () {
        beforeEach(function () {
          cy.get(as.dialogCreate.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as.dialogCreate.buttonCancel).should('not.be.visible');
        });
      });
    });
  }
});
