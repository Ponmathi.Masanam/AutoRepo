import "cypress/support/commands.js";

const AutomationStudio = require("cypress/integration/testcase/App-Automation_Studio/AutomationStudio.js");

describe("Automation Studio @2020.1", function () {
  let timeout;
  // Login to Application with valid credentials

  // Validate XHR Response
  describe("Validate XHR Response", function () {
    it("Validate XHR Response", function () {
      cy.get(AutomationStudio.homepage.automationstudiobutton, {
        timeout,
      }).should("be.visible");
      cy.server();
      cy.route("POST", "workflow_engine/getAssociatedJobs").as("validateapi");
      cy.SelectPageIcon("ACTIVE JOBS");
      cy.wait("@validateapi").its("status").should("eq", 200);
      cy.get("@validateapi").should((xhr) => {
        expect(xhr.method).to.equal("POST");
        const { body } = xhr.response;
        const totaljobscount = body.total;
        cy.get("#table_navigation > :nth-child(1)").should(
          "contains.text",
          totaljobscount
        );
      });
      cy.visit("/login?logout=true");
    });
  });
});
