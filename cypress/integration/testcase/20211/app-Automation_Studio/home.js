const model = {
  navbar: {
    appButton: 'span.ri.ri_view_app_grid.p-c.p-button-icon-left',
    applicationslink: 'span:contains("Applications")',
    briefcaseButton: 'span.ri.ri_briefcase.p-c.p-button-icon-left',
    homeButton: 'span.ri.ri_home.p-c.p-button-icon-left',
    activejobsmenu: 'span:contains("Active Jobs")',
    activetasksmenu: 'span:contains("Active Tasks")',
    jobmanagermenu: 'span:contains("Job Manager")',
    settingsicon: '#settings-title',
    AuthorizationSettingsMenu: 'span:contains("Authorization")',
  },
  page: {
    adminauthmessage: '.app-list',
    automationstudiolink: 'span:contains("Automation Studio")',
    goldenserviceslink: 'span:contains("Golden Services")',
    automationcataloglink: 'span:contains("Automation Catalog")',
    goldenconfiglink: 'span:contains("Golden Config")',
    configmgrlink: 'span:contains("Configuration Manager")',
    servicecataloglink: 'span:contains("Service Catalog Builder")',
    servicemgtlink: 'span:contains("Service Management")',
    nedvalidatorlink: 'span:contains("Ned Validator")',
    AuthManagerHeader: '#appName',
    AutomationCatalogButton: '[title="AUTOMATION CATALOG"]',
    pagebuttons: '.home-icon button',
    header: '.navbar-app-name',
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // Execute this module directly then output configuration.
  /* eslint-disable-next-line */
  console.log(JSON.stringify(model, null, 2));
} else {
  // Require this module and it will export.
  module.exports = model;
}
