import '../Commands/Commands';

const logIn = require('../pronghorn-core/logIn');
// const AutomationCatalog = require('./AutomationCatalog');
const home = require('./home');

// const $el = element.createElement;

describe('Automation Studio', function () {
  let timeout;
  // Login to Application with valid credentials
  describe('Validate Login as Admin role', function () {
    beforeEach(function () {
      timeout = 40000;
      cy.visit('/');
      const username = Cypress.env('IAP_USERNAME');
      const password = Cypress.env('IAP_PASSWORD');
      cy.get('input[name=username]').type(username);
      cy.get('input[name=password]').type(password);
      cy.get(logIn.signinButton).click();
    });
    // Validate Delete functionality of existing workflow
    describe('Delete of Existing Automation Catalog Workflow ', function () {
      it('should be performed successfully', function () {
        cy.get(home.page.AutomationCatalogButton, { timeout }).should(
          'be.visible'
        );
        cy.SelectPageIcon('AUTOMATION CATALOG');
        cy.ValidatePageHeader('Automation Catalog');
        cy.SelectWorkflow('newq');
        cy.visit('/login?logout=true');
      });
    });
  });
});
