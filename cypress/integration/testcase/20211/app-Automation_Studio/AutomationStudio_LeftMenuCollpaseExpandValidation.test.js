const logIn = require('../pronghorn-core/logIn');
const AutomationStudio = require('./AutomationStudio');

describe('Automation Studio @2020.1', function () {
  let timeout;
  // Login to Application with valid credentials
  describe('Automation Studio @2020.1', function () {
    beforeEach(function () {
      timeout = 40000;
      cy.visit('/');
          const username = Cypress.env('IAP_USERNAME');
      const password = Cypress.env('IAP_PASSWORD');
      cy.get('input[name=username]').type(username);
      cy.get('input[name=password]').type(password);
      
      cy.get(logIn.signinButton).click();
    });
    // Validate Automation Studio launch from Home Page
    describe('Automation Launch Point @2020.1', function () {
      it('should be displayed in Home Page.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.visit('/login?logout=true');
      });
    });
    // Validate Expand and Collapse icon of Left Menu in Automation Studio Page
    describe('Clicking Expand and Collpase menu of Left Menu @2020.1 ', function () {
      it('Should hide and unhide the links.', function () {
        cy.get(AutomationStudio.homepage.automationstudiobutton, {
          timeout,
        }).should('be.visible');
        cy.get(AutomationStudio.homepage.automationstudiobutton).click();
        cy.get(AutomationStudio.automationstudiopage.welcomemessage, {
          timeout,
        }).should('be.visible');
        cy.get(
          AutomationStudio.automationstudiopage.AutomationsNavBarLink
        ).should('be.visible');
        cy.get(AutomationStudio.automationstudiopage.Collapseicon).click();
        cy.get(
          AutomationStudio.automationstudiopage.AutomationsNavBarLink
        ).should('not.be.visible');
        cy.get(AutomationStudio.automationstudiopage.Expandicon).click();
        cy.get(
          AutomationStudio.automationstudiopage.AutomationsNavBarLink
        ).should('be.visible');
        cy.visit('/login?logout=true');
      });
    });
  });
});
