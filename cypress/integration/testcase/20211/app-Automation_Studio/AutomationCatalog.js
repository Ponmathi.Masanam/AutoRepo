const model = {
  sectionbar: {
    automationcataloglink: 'span:contains("Automation Catalog")',
  },
  page: {
    automationsheader: 'span:contains("Automations")',
    createautomationicon:
      '.ph-accordion-header-right > :nth-child(1) > .p-button > .ri',
    createautomationbutton: 'span.ri.ri_add.p-c.p-button-icon-left',
    header: '.navbar-app-name',
    viewmodulegrid: 'span.ri.ri_view_module.p-c.p-button-icon-left',
    existingworkflow: '.p-treenode-label',
  },
  createautomationcomponent: {
    cancelbutton: 'span:contains("Cancel")',
    desccriptionlabel: 'h3:contains("Description")',
    namelabel: 'h3:contains("Name")',
    nameinput: '.p-dialog-content > .ph-input',
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // Execute this module directly then output configuration.
  /* eslint-disable-next-line */
  console.log(JSON.stringify(model, null, 2));
} else {
  // Require this module and it will export.
  module.exports = model;
}
