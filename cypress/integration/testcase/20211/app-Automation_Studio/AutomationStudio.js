const model = {
  homepage: {
    automationstudiobutton: '[title="AUTOMATION STUDIO"]',
  },
  automationstudiopage: {
    welcomemessage: 'h3:contains("Get")',
    AutomationsNavBarLink: 'span:contains("Automations")',
    FormsNavBarLink: 'span:contains("Forms")',
    CommandTemplateslink: 'span:contains("Command Templates")',
    AnalyticTemplateslink: 'span:contains("Analytic Templates")',
    Templateslink: 'span:contains("Templates")',
    AddAutomationsIcon:
      '.ph-accordion-header-right > :nth-child(1) > .p-button',
    GridViewIcon:
      '.ph-accordion-header-right > :nth-child(2) > .p-button > .ri',
    GridViewPageHeader: '.ph-title-bar-overwrite',
    Collapseicon: 'span:contains("COLLAPSE")',
    Expandicon: '.ph-footer-left > div > .p-button > .ri',
  },
  createautomationcomponent: {
    header: '#phui-modal-title',
    cancelbutton: '.Cancel > label',
  },
  automations: {
    gridviewfilterdropdown: 'label:contains("Name")',
    refreshicon: '.css-1jfc56c > :nth-child(1)',
    Refreshicon: '.ri.ri_redo.p-c.p-button-icon-left',
    Importicon: '.ri.ri_import.p-c.p-button-icon-left',
    SelectAllicon: '.ri.ri_check_rounded_circle_outline.p-c.p-button-icon-left',
    Deleteicon: '.ri.ri_trash.p-c.p-button-icon-left',
    Exporticon: '.ri.ri_export.p-c.p-button-icon-left',
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // Execute this module directly then output configuration.
  /* eslint-disable-next-line */
  console.log(JSON.stringify(model, null, 2));
} else {
  // Require this module and it will export.
  module.exports = model;
}
