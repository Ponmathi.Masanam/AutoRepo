import '../Commands/Commands';

const logIn = require('../pronghorn-core/index');
// const AutomationStudio = require('./AutomationStudio');

describe('Automation Studio @2020.1', function () {
  // let timeout;
  // Login to Application with valid credentials
  describe('Automation Studio @2020.1', function () {
    beforeEach(function () {
      // timeout = 40000;
      cy.visit('/');
          const username = Cypress.env('IAP_USERNAME');
      const password = Cypress.env('IAP_PASSWORD');
      cy.get('input[name=username]').type(username);
      cy.get('input[name=password]').type(password);
      
      cy.get(logIn.signinButton).click();
    });
    // Validate XHR Response
    describe('Validate XHR Response', function () {
      it('Validate XHR Response', function () {
        cy.server();
        cy.route('POST', 'workflow_engine/getAssociatedJobs').as('validateapi');
        cy.SelectPageIcon('ACTIVE JOBS');
        cy.wait('@validateapi').its('status').should('eq', 200);
        cy.get('@validateapi').should((xhr) => {
          expect(xhr.method).to.equal('POST');
          const { body } = xhr.response;
          const totaljobscount = body.total;
          cy.get('#table_navigation > :nth-child(1)').should(
            'contains.text',
            totaljobscount
          );
        });
        cy.visit('/login?logout=true');
      });
    });
  });
});
