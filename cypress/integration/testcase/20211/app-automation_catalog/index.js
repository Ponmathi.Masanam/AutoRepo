const model = {
  url: '/automation_catalog',

  HomeIcon: '.ph-legacy-navbar-btn-1 > div',
  LeftMenuAutomationCatalogLink: 'a:contains("Automation Catalog")',
  ApplicationsMenulink: '.ri.ri_menu.p-c.p-button-icon-left',
  Applicationslink: 'span:contains("Applications")',
  AutomationCataloglink: 'span:contains("Automation Catalog")',

  title: 'div.navbar-app-name:contains("Automation Catalog")',

  buttonCreate:
    'div.ph-collapse-panel div.panel-wrap div.panel-content div.toolbar-button button.p-button span.ri_add_circle',
  buttonCreateAnAutomation:
    'button.p-button span.p-button-text:contains("Create an automation")',
  buttonFindAnAutomation:
    'button.p-button span.p-button-text:contains("Find an automation")',
  dialogCollection: {
    buttonClose:
      'div.p-dialog-content button.p-button span.p-button-text:contains("CLOSE")',
    tabscollection: '.css-dijvuc',
    tabs: '.css-1jhtmls',
    SearchTextbox:
      '.css-cf6p6w > .ph-search-container > .ph-search > .p-autocomplete > .p-inputtext',
    SearchIcon:
      '.css-cf6p6w > .ph-search-container > .ph-search > .ph-search-icon',
    SearchResultWFName: 'h3 > a',
    ClassicWFIcon: '.ph-card__header > .ri',
    ZeroTotalCount: '.css-2rvv6y > .ph-text',
    SelectAllWF: ':nth-child(3) > .ri',
    DeleteIcon: '.ri.ri_trash.p-c.p-button-icon-left',
    DeleteDialogHeader: '#pr_id_14_header',
    buttonCancelDelete: 'span:contains("cancel")',
    buttonCancelClone: 'span:contains("Cancel")',
    Menulist:
      '.css-147x205 > .ph-popover-container > .ph-trigger-component > .p-button > .ri',
    Clone: 'span:contains("Clone")',
    CloneDialogHeader: 'span:contains("Clone Automation")',
    Edit: 'span:contains("Edit")',
    EditWFHeader: '.pointer > #cntr > #cntr-header > .wf-title',
  },

  dialogCreate: {
    CreateAutomationHeader:'#pr_id_2_header',
    NameHeader:'h3:contains("Name")',
    DescriptionHeader:'h3:contains("Description")',
    NameInput:'.p-dialog-content > .ph-input',
    DescriptionInput:'.ph-text-area-container > .p-inputtext',
    buttonCancel:
      'span:contains("Cancel")',
    buttonCreate:
      'span:contains("Create")',
  },
  20201: {
    automations: {
      title: 'h1.ph-title-bar-overwrite:contains("Automations")',
    },
    dialogCreateAutomation: {
      buttonCancel: 'button.Cancel',
    },
  },
  20202: {
    automations: {
      title: 'h1.ph-title-bar-overwrite:contains("Automations")',
    },
    dialogCreateAutomation: {
      buttonCancel: 'button.Cancel',
    },
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // Execute this module directly then output configuration.
  /* eslint-disable-next-line */
  console.log(JSON.stringify(model, null, 2));
} else {
  // Require this module and it will export.
  module.exports = model;
}
