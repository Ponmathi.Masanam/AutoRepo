const as = require('.');

describe('Given a user to navigate Automation Catalog', function () {
  it('should click its link in Operations left menu of Home Page.', function () {
    cy.visit('/');
    cy.get(as.AutomationCataloglink).click();
    cy.get(as.title).should('be.visible');
  });

  it('should click its link in Applications list menu of Home Page.', function () {
    cy.visit('/');
    cy.get(as.ApplicationsMenulink).click();
    cy.get(as.Applicationslink).click();
    cy.get(as.AutomationCataloglink).first().click();
    cy.get(as.title).should('be.visible');
  });

  it('should use direct url.', function () {
    cy.visit(as.url);
    cy.get(as.title).should('be.visible');
  });
});

describe('Given a user visits Automation Catalog', function () {
  before(function () {
    this.iapRelease = Cypress.env('IAP_RELEASE');
  });

  beforeEach(function () {
    cy.visit(as.url);
  });

  describe('click Create An Automation button', function () {
    beforeEach(function () {
      cy.get(as.dialogCreate.buttonCreateAnAutomation).click();
    });

    if (this.iapRelease !== '2020.2') {
      it('should reveal the Create dialog.', function () {
        cy.get(as.dialogCreate.CreateAutomationHeader).should('be.visible');
      });

      it('should display the create button in disabled mode.', function () {
        cy.get(as.dialogCreate.buttonCreate).should('be.disabled');
      });

      describe('click Cancel button', function () {
        beforeEach(function () {
          cy.get(as.dialogCreate.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as.dialogCreate.buttonCancel).should('not.be.visible');
        });
      });

      describe('Create Automation Catalog Workflow', function () {
        it('should be done using Create Automation Dialog.', function () {
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          const uuid = () => Cypress._.random(0, 1e6)
          const id = uuid()
          const workflowname = `AutoCatalog${id}`
          cy.get(as.dialogCreate.NameInput).type(workflowname);
          cy.get(as.dialogCreate.buttonCreate).click();
          // eslint-disable-next-line cypress/no-unnecessary-waiting
          cy.wait(2000);
          cy.get(as.AutoCatalogPage.AutoCatalogWFtitle).should('have.text', workflowname);
        });
      });

    } else {
      it('should reveal the Create Automaton dialog.', function () {
        cy.get(as['20201'].dialogCreateAutomation.buttonCancel).should(
          'be.visible'
        );
      });

      describe('click Cancel button @ph78144', function () {
        beforeEach(function (done) {
          // Make exception not fail this test. At least until PH-78144 is addressed.
          cy.on('uncaught:exception', (error) => {
            expect(error.message).to.include("Failed to execute 'removeChild'");
            done();
            return false;
          });
          cy.get(as['20201'].dialogCreateAutomation.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as['20201'].dialogCreateAutomation.buttonCancel).should(
            'not.be.visible'
          );
        });
      });
    }
  });

  if (this.iapRelease === '2020.2') {
    describe('click Create button', function () {
      beforeEach(function () {
        cy.get(as.buttonCreate).click();
      });

      it('should reveal the Create dialog.', function () {
        cy.get(as.dialogCreate.buttonCancel).should('be.visible');
      });

      describe('click Cancel button', function () {
        beforeEach(function () {
          cy.get(as.dialogCreate.buttonCancel).click();
        });

        it('should hide the Create dialog.', function () {
          cy.get(as.dialogCreate.buttonCancel).should('not.be.visible');
        });
      });
    });
  }
});
