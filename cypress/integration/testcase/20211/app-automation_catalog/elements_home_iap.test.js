/** Automation Catalog related elements should be visible on the IAP Home page.
 * @module
 */

const ac = require('.');

describe('[functional] app-automation_catalog', function () {
  describe('Automation Catalog releated elements should be visible on the IAP Home Page.', function () {
    describe('Applications menu item, Automation Catalog,', function () {
      it('should not be visible because the Applications menu has not been clicked yet.', function () {
        cy.get(ac.iap.home.applications.elements.automationCatalog).should(
          'not.be.visible'
        );
      });
    });

    describe('Click the Applications menu and Automation Catalog menu item', function () {
      it('should be visible.', function () {
        cy.get(ac.iap.home.applications.element).click();
        cy.get(ac.iap.home.applications.elements.automationCatalog).should(
          'be.visible'
        );
      });
    });

    describe('Automation Catalog icon', function () {
      it('should be visible.', function () {
        cy.get(ac.iap.home.icons.automationCatalog).should('be.visible');
      });
    });
  });
});
